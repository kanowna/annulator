annulator V0.0.2
================

Intro
-----
Here's some info about building & running **annulator** utility.

* An open-source CAD tool, for 3D modelling.
* Annulator creates an embossed texture-roller, useful for pottery, baking, crafts, etc.
* Accepts a grayscale [PNG](https://en.wikipedia.org/wiki/Portable_Network_Graphics) image, as texture input.
* Generates an ASCII [STL](https://en.wikipedia.org/wiki/STL_(file_format)) file, ready for 3D printing.

Examples
--------
Here is a PNG bitmap, consisting of diagonal stripes.  It was made with GNU Octave tool.

![stripes](./images/stripes.png)

And here is the generated STL solid-model, whose surface is embossed with the diagonal stripe texture.

![stripes](./images/stripes_solid.png)

Here is a PNG bitmap, displaying an image of the Coronavirus.

![coronavirus](./images/coronavirus.png)

And here is the generated STL solid-model, whose surface is embossed with the Coronavirus texture.

![coronavirus](./images/coronavirus_solid.png)

You get the general idea.

Please note: The supplied PNG image is assumed to contain a "Grayscale" image, as the code stands right now.

Prerequisites
-------------
* C++ compiler
* cmake (version 3.12.0 or better)
* BOOST library
* PNG library

Compiling
---------
The following instructions should work OK on most Linux (and \*nix) platforms.
But it shouldn't be too difficult to build it on Mac or Win32, etc also.

* cd _working directory_
* git clone https://bitbucket.org/kanowna/annulator.git
* cd annulator
* mkdir build
* cd build
* cmake ..
* make

Running
-------

> ./annulator --help

For example....
> ./annulator --input-file ../images/stripes.png --output-file stripes.stl

> openscad stripes.stl

> ./annulator --input-file ../images/coronavirus.png --output-file coronavirus.stl --texture-gain 0.02

> openscad coronavirus.stl


Todo
----
* Annulator project is very new, and immature.
* There's a heap of bugs and gotchas.
* I'd cheerfully welcome help anybody who wishes to contribute.
