cmake_minimum_required(VERSION 3.21.4)

project(annulator)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_BUILD_TYPE Release)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)
set(CMAKE_INCLUDE_CURRENT_DIR_IN_INTERFACE ON)

set(Boost_USE_STATIC_LIBS ON) 
set(Boost_USE_MULTITHREADED ON)  
set(Boost_USE_STATIC_RUNTIME OFF)

find_package(Boost REQUIRED COMPONENTS system program_options)
find_package(PNG REQUIRED)
message(${Boost_LIBRARIES})
message(${PNG_LIBRARIES})
message(${PNG_INCLUDE_DIR})

include_directories(${PNG_INCLUDE_DIR} CMakeProject2 PUBLIC ${Boost_INCLUDE_DIRS}) 

add_executable(${PROJECT_NAME} annulator.cpp Annulus.cpp)
target_link_libraries(${PROJECT_NAME} ${PNG_LIBRARY} ${Boost_LIBRARIES})

install(TARGETS ${PROJECT_NAME} DESTINATION bin)
