#include <boost/geometry.hpp>
#include <complex>
#include <cstdint>
#include <png++/png.hpp>
#include <vector>

namespace bg = boost::geometry;
typedef bg::model::point<double, 3, bg::cs::cartesian> point3d;

class Annulus
{
public:
  Annulus(const png::image<png::gray_pixel>& bitmap,
          const double radiusOuter = NAN,
          const double radiusInner = NAN,
          const double textureGain = NAN);

  uint32_t getHeight();
  uint32_t getWidth();
  point3d getPoint3d(const uint32_t j, const uint32_t i);
  bool isValid();

private:
  typedef std::complex<double> PolarType; 

  png::image<png::gray_pixel> m_bitmap;
  double m_cylinderHeight;
  double m_heightDelta;
  uint32_t m_modelHeight;
  uint32_t m_modelWidth;
  std::vector<PolarType> m_polar;
  double m_radiusInner;
  double m_radiusMedian;
  double m_radiusOuter;
  double m_textureGain;
  bool m_valid;
};
