#include "Annulus.h"
#include "version.h"
#include <boost/program_options.hpp>
#include <cmath>
#include <fstream>
#include <iostream>
#include <limits>
#include <png++/png.hpp>
#include <vector>

namespace po = boost::program_options;

typedef std::vector<point3d> SurfaceType;

void appendQuad(const point3d& vtx0,
                const point3d& vtx1,
                const point3d& vtx2,
                const point3d& vtx3,
                SurfaceType& surface)
{
  surface.push_back(vtx0);
  surface.push_back(vtx2);
  surface.push_back(vtx3);

  surface.push_back(vtx3);
  surface.push_back(vtx1);
  surface.push_back(vtx0);
}

void drawModel(Annulus& model, SurfaceType& surface)
{
  const uint32_t modelHeight = model.getHeight();
  const uint32_t modelWidth = model.getWidth();
//std::cout << "drawModel(...) : " << modelHeight << " : " << modelWidth <<std::endl;

  for (uint32_t i0=0; i0<modelWidth; ++i0)
  {
    uint32_t i1 = (i0 + 1) % modelWidth;

    for (uint32_t j0=0; j0<modelHeight; ++j0)
    {
      uint32_t j1 = (j0 + 1) % modelHeight;
  
//~~  std::cout << "**drawModel(...) "
//~~            << "[" << j0 << "-" << j1 << "]"
//~~       	    << "[" << i0 << "-" << i1 << "]"
//~~            << std::endl;

      point3d p01 = model.getPoint3d(i0, j1);
      point3d p11 = model.getPoint3d(i1, j1);
      point3d p00 = model.getPoint3d(i0, j0);
      point3d p10 = model.getPoint3d(i1, j0);

      appendQuad(p01, p11, p00, p10, surface);
    }
  }
}

point3d operator - (const point3d& a, const point3d& b)
{
  // Subtraction.
  return point3d(a.get<0>() - b.get<0>(),
                 a.get<1>() - b.get<1>(),
                 a.get<2>() - b.get<2>());
}

point3d operator ^ (const point3d& a, const point3d& b)
{
  // Cross product.
  return point3d(a.get<1>() * b.get<2>() - a.get<2>() * b.get<1>(),
                 a.get<2>() * b.get<0>() - a.get<0>() * b.get<2>(),
                 a.get<0>() * b.get<1>() - a.get<1>() * b.get<0>());
}

void unit(point3d& p)
{
  // Normalize a 3D Cartesian point as vector.
  const point3d ZERO(0, 0, 0);
  double norm = bg::distance(p, ZERO);
//std::cout << "unit(" << p.get<0>() << ", " << p.get<1>() << ", " << p.get<2>() << ") => " << norm << std::endl;
  if (norm > std::numeric_limits<double>::epsilon())
  {
    p.set<0>(p.get<0>() / norm);
    p.set<1>(p.get<1>() / norm);
    p.set<2>(p.get<2>() / norm);
//  std::cout << "**unit(...) => {" << p.get<0>() << ", " << p.get<1>() << ", " << p.get<2>() << "}" << std::endl;
  }
}

void stlFacetOut(std::ofstream& stlout, std::vector<point3d>::const_iterator vv, point3d normal)
{
// Export an STL facet to an output stream, stlout.

  stlout << "facet normal " << normal.get<0>() << " " << normal.get<1>() << " " << normal.get<2>() << std::endl;
  stlout << "   outer loop" << std::endl;
  stlout << "    vertex " << vv[0].get<0>() << " " << vv[0].get<1>() << " " << vv[0].get<2>() << std::endl;
  stlout << "    vertex " << vv[1].get<0>() << " " << vv[1].get<1>() << " " << vv[1].get<2>() << std::endl;
  stlout << "    vertex " << vv[2].get<0>() << " " << vv[2].get<1>() << " " << vv[2].get<2>() << std::endl;
  stlout << "   std::endloop" << std::endl;
  stlout << "endfacet" << std::endl;
}

void stlExport(std::ofstream& stlout, const std::vector<point3d>& surface)
{
// Export in STL format, to an output stream, stlout.
//std::cout << "stlExport(...) : surface.size() = " << surface.size() << std::endl;

  point3d normal;

  stlout << "solid stlExport" << std::endl;

  for (auto vtxIter = surface.begin(); vtxIter < surface.end(); vtxIter+=3)
  {
    normal = (vtxIter[1] - vtxIter[0]) ^ (vtxIter[2] - vtxIter[1]);
    unit(normal);
    stlFacetOut(stlout, vtxIter, normal);
  }

  stlout << "endsolid stlExport" << std::endl;
}

int main(int argc, char** argv)
{
  try
  {
  	// Declare the supported CLI options.
  	std::string inputPath;
  	std::string outputPath("output.stl");
    double radiusInner = NAN;
    double radiusOuter = NAN;
    double textureGain = NAN;

  	std::string version(ANNULATOR_APPNAME + std::string(" Version: ") + ANNULATOR_VERSION_STRING);
  	po::options_description description("Usage:\n[--help]\nAllowed options");
  	description.add_options()
  		("help", "Produce help message")
  		("input-file", po::value<std::string>(&inputPath)->required(), "Input (PNG) path")
  		("output-file", po::value<std::string>(&outputPath), "Output (STL) path")
      ("radius-inner", po::value<double>(&radiusInner), "Spindle radius (mm)")
      ("radius-outer", po::value<double>(&radiusOuter), "Annular radius (mm)")
      ("texture-gain", po::value<double>(&textureGain), "Output (STL) path");
  
  	po::variables_map varMap;
  	po::store(po::parse_command_line(argc, argv, description), varMap);
  	po::notify(varMap);  

  	std::cout << "main(...) : inputPath{" << inputPath << "}" << std::endl;
  	std::cout << "main(...) : outputPath{" << outputPath << "}" << std::endl;
  
  	if (varMap.count("help"))
  	{
  		std::cout << version << std::endl;
  		std::cout << description << std::endl;
  		return 1;
  	}

  	// Create annulus model.
  	png::image<png::gray_pixel> bitmap(inputPath);
  	//std::cout << "main(...) : bitmap{" << bitmap.get_height() << " x " << bitmap.get_width() << "}" << std::endl;
    Annulus model(bitmap, radiusInner, radiusOuter, textureGain);
//~~Annulus model(bitmap, RADIUS_OUTER, RADIUS_INNER);
  
  	if (model.isValid())
  	{
  		// Draw the model surface.
  		SurfaceType surface;
  		surface.reserve(model.getHeight() * model.getWidth());
  		drawModel(model, surface);
  		std::cout << "main(...) : surface.size()=" << surface.size() << "/" << surface.capacity() << std::endl;
  
  		// Export the surface, in text-based STL format.
  		std::ofstream stlout(outputPath, std::ofstream::trunc);
  		stlExport(stlout, surface);
  		stlout.close();
  	}
  
  	else
  	{
  		std::cerr << "ERROR: Invalid model parameters" << std::endl;
  	}
  }
  
  catch(po::error& e)
  {
  	std::cout << e.what() << std::endl;
  }

  return 0;
}
