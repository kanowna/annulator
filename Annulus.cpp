#include "Annulus.h"
#include <cmath>
#include <iostream>

const uint32_t BITMAP_MIN_PIXELS = 32;      ///< Minimum acceptable pixel count.
const double DEFAULT_RADIUS_INNER = 4.0;    ///< Spindle nominal radius (mm).
const double DEFAULT_RADIUS_OUTER = 38.0;   ///< Nominal annulus radius (mm).
const double DEFAULT_TEXTURE_GAIN = 0.028;  ///< Greyscale=>texture gain-ratio.

Annulus::Annulus(const png::image<png::gray_pixel>& bitmap,
                 const double radiusOuter,
                 const double radiusInner,
                 const double textureGain)
: m_bitmap(bitmap),
  m_cylinderHeight(NAN),
  m_heightDelta(NAN),
  m_modelHeight(bitmap.get_height() + 4),
  m_modelWidth(bitmap.get_width()),
  m_radiusInner(radiusInner),
  m_radiusOuter(radiusOuter),
  m_textureGain(textureGain),
  m_valid(false)
{
  if ((bitmap.get_height() >= BITMAP_MIN_PIXELS) &&
    (bitmap.get_width() >= BITMAP_MIN_PIXELS))
  {
    std::cout << "Annulus::Annulus(...) : " << bitmap.get_width() << " : " << bitmap.get_height() << std::endl;
    if (isnan(radiusInner))
    {
      m_radiusInner = DEFAULT_RADIUS_INNER;
    }

    if (isnan(radiusOuter))
    {
      m_radiusOuter = DEFAULT_RADIUS_OUTER;
    }

    if (isnan(textureGain))
    {
      m_textureGain = DEFAULT_TEXTURE_GAIN;
    }

    m_radiusMedian = (m_radiusInner + m_radiusOuter) / 2;

    double aspectRatio = bitmap.get_width() / bitmap.get_height();
    double segmentAngle = 2 * M_PI / bitmap.get_width();     
    m_cylinderHeight = 2 * M_PI * m_radiusOuter / aspectRatio; 
    m_heightDelta = m_cylinderHeight / bitmap.get_height();
    m_polar.reserve(bitmap.get_width());
    m_valid = true;

    for (uint32_t i=0; i<bitmap.get_width(); ++i)
    {
      double theta = i * segmentAngle;
      m_polar[i] = PolarType(cos(theta), sin(theta));
      PolarType polar(cos(theta), sin(theta));
    }
  }
}

uint32_t Annulus::getHeight()
{
  return m_modelHeight;
}

uint32_t Annulus::getWidth()
{
  return m_modelWidth;
}

point3d Annulus::getPoint3d(const uint32_t i, const uint32_t j)
{
  point3d result{NAN, NAN, NAN};

  if ((i < m_modelWidth) &&
      (j < m_modelHeight))
  {
    if (j == 0)
    {
      result.set<0>(m_radiusInner * m_polar[i].real());
      result.set<1>(m_radiusInner * m_polar[i].imag());
      result.set<2>(0.0);
    }

    else if (j == 1)
    {
      result.set<0>(m_radiusMedian * m_polar[i].real());
      result.set<1>(m_radiusMedian * m_polar[i].imag());
      result.set<2>(0.0);
    }

    else if (j == m_modelHeight - 2)
    {
      result.set<0>(m_radiusMedian * m_polar[i].real());
      result.set<1>(m_radiusMedian * m_polar[i].imag());
      result.set<2>(m_cylinderHeight);
    }

    else if (j == m_modelHeight - 1)
    {
      result.set<0>(m_radiusInner * m_polar[i].real());
      result.set<1>(m_radiusInner * m_polar[i].imag());
      result.set<2>(m_cylinderHeight);
    }

    else
    {
      double radius = m_radiusOuter - m_textureGain * m_bitmap[j-2][i];
      result.set<0>(radius * m_polar[i].real());
      result.set<1>(radius * m_polar[i].imag());
      result.set<2>((j - 2) * m_heightDelta);
    }
  }

  return result;
}

bool Annulus::isValid()
{
  return m_valid;
}
